#include <iostream>
#include <vector>
#include "Row.cpp"
#include "Attribute.cpp"
#include <fstream>
#include <string>
#include <sstream>
#include <utility>

using namespace std;

class File {
	
	vector<string> explode(const string& str, const char& ch) {
	    string next;
	    vector<string> result;
	
	    // For each character in the string
	    for (string::const_iterator it = str.begin(); it != str.end(); it++) {
	        // If we've hit the terminal character
	        if (*it == ch) {
	            // If we have some characters accumulated
	            if (!next.empty()) {
	                // Add them to the result vector
	                result.push_back(next);
	                next.clear();
	            }
	        } else {
	            // Accumulate the next character into the sequence
	            next += *it;
	        }
	    }
	    if (!next.empty())
	         result.push_back(next);
	    return result;
	}
	
	public: void getAttributes()
	{
		ifstream file ("data.txt");
		
		string str;
		getline(file, str);
		
		vector<string> result = this->explode(str, ' ');
		
	    for (size_t i = 0; i < result.size(); i++) {
	        cout << "\"" << result[i] << "\"" << endl;
	    }	
	}
	
	public: void getContent()
	{	
		ifstream file ("data.txt");
		
		string str; 
		string fileContents;
		
	    while (getline(file, str))
	    {
	        fileContents += str;
	  		fileContents.push_back('\n');
	    }
	    
	    cout << fileContents;
	}
		
};
